import os
from flask import Flask, render_template, request

import smtplib
import logging
import threading
import time
import datetime as dt
import time
import smtplib
import sqlite3

conn = sqlite3.connect('resume_IT_db.sqlite')

cur = conn.cursor()

conn.commit()

global mailing_list
mailing_list = []

global total_sum
total_sum = 0
import math
import PyPDF2
import os

from io import StringIO
import pandas as pd
from collections import Counter
#import en_core_web_sm
import spacy
from spacy.matcher import PhraseMatcher
nlp = spacy.blank('en')

from flask import Flask, render_template, request
import random, copy


original_questions = {
 #Format is 'question':[options]
 'Taj Mahal':['Agra','New Delhi','Mumbai','Chennai'],
 'Great Wall of China':['China','Beijing','Shanghai','Tianjin'],
 'Petra':['Ma\'an Governorate','Amman','Zarqa','Jerash'],
 'Machu Picchu':['Cuzco Region','Lima','Piura','Tacna'],
 'Egypt Pyramids':['Giza','Suez','Luxor','Tanta'],
 'Colosseum':['Rome','Milan','Bari','Bologna'],
 'Christ the Redeemer':['Rio de Janeiro','Natal','Olinda','Betim']
}



data_science_mcq = {	'	Point out the wrong statement.	'	:	[	'	 Data visualization is the organization of information according to preset specifications	'	,	'	 Merging concerns combining datasets on the same observations to produce a result with more variables	'	,	'	 Subsetting can be used to select and exclude variables and observations	'	,	'	 All of the mentioned	'	]	,
	'	Which of the following is characteristic of Raw Data?	'	:	[	'	 Original version of data	'	,	'	 Data is ready for analysis	'	,	'	 Easy to use for data analysis	'	,	'	 None of the mentioned	'	]	,
	'	Which of the following CLI command can also be used to rename files?	'	:	[	'	 mv	'	,	'	 rm	'	,	'	 rm -r	'	,	'	 none of the mentioned	'	]	,
	'	Which of the following command updates tracking for files that are modified?	'	:	[	'	 git add -u	'	,	'	 git add .	'	,	'	 git add -A	'	,	'	 none of the mentioned	'	]	,
	'	Which of the following is the common goal of statistical modelling?	'	:	[	'	 Inference	'	,	'	 Summarizing	'	,	'	 Subsetting	'	,	'	 None of the mentioned	'	]	,
	'	Which of the following analysis is usually modeled by deterministic set of equations?	'	:	[	'	 Mechanistic	'	,	'	 Causal	'	,	'	 Predictive	'	,	'	 All of the mentioned	'	]	,
	'	Point out the correct statement.	'	:	[	'	 None of the mentioned	'	,	'	 Data Cleaning focuses on prediction, based on known properties learned from the training data	'	,	'	 Representing data in a form which both mere mortals can understand and get valuable insights is as much a science as much as it is art	'	,	'	 Machine learning focuses on prediction, based on known properties learned from the training data	'	]	,
	'	 3V’s are not sufficient to describe big data.	'	:	[	'	 True	'	,	'	 False	'	,	'	maybe	'	,	'	none	'	]	,
	'	Point out the wrong combination with regards to kind keyword for graph plotting.	'	:	[	'	 ‘kde’ for hexagonal bin plots	'	,	'	 ‘scatter’ for scatter plots	'	,	'	 ‘pie’ for pie plots	'	,	'	 none of the mentioned	'	]	,
	'	Which of the following method is used for trainControl resampling?	'	:	[	'	 repeatedcv	'	,	'	 svm	'	,	'	 bag32	'	,	'	 none of the mentioned	'	]	}



java_lang_mcq = {	'	Which of this is not a bitwise operator?	'	:	[	'	"	<=\' Operator"	'	,	'	&\' Operator	'	,	'	|=\' Operator	'	,	'	&=\' Operator	'	]	,
	'	Modulus operator (%) can be applied to which of these?	'	:	[	'	Both of them	'	,	'	Integers	'	,	'	Float	'	,	'	 None of the mentioned	'	]	,
	'	Which of the following is not a Java features?	'	:	[	'	 Use of pointers	'	,	'	Dynamic	'	,	'	Archiecture netural	'	,	'	Object-oriented	'	]	,
	'	The \u0021 article referred to as a	'	:	[	'	Unicode escape sequence	'	,	'	Octal escape	'	,	'	Hexadecimal	'	,	'	Line feed	'	]	,
	'	Which of the following is a valid declaration of a char?	'	:	[	'	char ch = \'\\utea\';	'	,	'	 char ca = \'tea\';	'	,	'	 char cr = \u0223;	'	,	'	char cc = \'\itea\';	'	]	}




questions = copy.deepcopy(original_questions)
data_science_questions = copy.deepcopy(data_science_mcq)
java_lang_questions = copy.deepcopy(java_lang_mcq)

def shuffle(q):
 """
 This function is for shuffling
 the dictionary elements.
 """
 selected_keys = []
 i = 0
 while i < len(q):
  current_selection = random.choice(list(q.keys()))
  if current_selection not in selected_keys:
   selected_keys.append(current_selection)
   i = i+1
 return selected_keys
















app = Flask(__name__)
# adding dummy comment

def pdfextract(file):
    fileReader = PyPDF2.PdfFileReader(open(file,'rb'))
    countpage = fileReader.getNumPages()
    count = 0
    text = []
    while count < countpage:    
        pageObj = fileReader.getPage(count)
        count +=1
        t = pageObj.extractText()
       # print (t)
        text.append(t)
    #print('text ',text)
    return text

def service_2_profile(file, target_job_title):
    # converting the resume pdf into text
    text = pdfextract(file) 
    text = str(text)
    text = text.replace("\\n", "")
    text = text.lower()
    
    #below is the csv where we have all the keywords
    if target_job_title == 'DS':
        keyword_dict = pd.read_csv('data_science_keywords.csv')
    elif target_job_title == 'WD':
        keyword_dict = pd.read_csv('web_developer_keywords.csv')
    elif target_job_title == 'ISA':
        keyword_dict = pd.read_csv('information_security_analyst.csv')
    elif target_job_title == 'SE':
        keyword_dict = pd.read_csv('software_engineer_keywords.csv')
    elif target_job_title == 'AI':
        keyword_dict = pd.read_csv('ai_ml_keywords.csv')
        
    
    keyword_total = list(keyword_dict.count())
    total_sum = 0
    for i in keyword_total:
        total_sum = total_sum + i
        
    keyword_dict_col_names = list(keyword_dict.columns)

    # ======================================================
    final = []
    for c in keyword_dict_col_names:
        final = keyword_dict[c].tolist() + final

    final2 = []
    for i in final:
        if type(i) == str:
            final2.append(i)

    print(final2)

   
    matcher = PhraseMatcher(nlp.vocab)
    
    for col in keyword_dict_col_names: 
        matcher.add(col, None, *[nlp(text) for text in keyword_dict[col].dropna(axis = 0)])
    
    doc = nlp(text)
   # print('doc',doc)
    
    d = []  
    matches = matcher(doc)
    for match_id, start, end in matches:
        rule_id = nlp.vocab.strings[match_id]  # get the unicode ID, i.e. 'COLOR'
        span = doc[start : end]  # get the matched slice of the doc
        d.append((rule_id, span.text))  
    keywords = "\n".join(f'{i[0]} {i[1]} ({j})' for i,j in Counter(d).items())
    
    ## convertimg string of keywords to dataframe
    df = pd.read_csv(StringIO(keywords),names = ['Keywords_List'])
    df1 = pd.DataFrame(df.Keywords_List.str.split(' ',1).tolist(),columns = ['Subject','Keyword'])
    df2 = pd.DataFrame(df1.Keyword.str.split('(',1).tolist(),columns = ['Keyword', 'Count'])
    df3 = pd.concat([df1['Subject'],df2['Keyword'], df2['Count']], axis =1) 
    df3['Count'] = df3['Count'].apply(lambda x: x.rstrip(")"))
    
    base = os.path.basename(file)
    filename = os.path.splitext(base)[0]
       
    name = filename.split('_')
    name2 = name[0]
    name2 = name2.lower()
    ## converting str to dataframe
    name3 = pd.read_csv(StringIO(name2),names = ['Candidate Name'])
    
    dataf = pd.concat([name3['Candidate Name'], df3['Subject'], df3['Keyword'], df3['Count']], axis = 1)
    dataf['Candidate Name'].fillna(dataf['Candidate Name'].iloc[0], inplace = True)
    print('dataf ',type(dataf))
    print('total_sum ',total_sum)
    print('final ',final2)
    ttt=(dataf,total_sum,final2 )
    print ('rrr ', ttt)
    return ttt
    



def service_1_profile(file, target_job_title):
    # converting the resume pdf into text
    text = pdfextract(file) 
    text = str(text)
    text = text.replace("\\n", "")
    text = text.lower()
    
    #below is the csv where we have all the keywords
    if target_job_title == 'DS':
        keyword_dict = pd.read_csv('data_science_keywords.csv')
    elif target_job_title == 'WD':
        keyword_dict = pd.read_csv('web_developer_keywords.csv')
    elif target_job_title == 'ISA':
        keyword_dict = pd.read_csv('information_security_analyst.csv')
    elif target_job_title == 'SE':
        keyword_dict = pd.read_csv('software_engineer_keywords.csv')
    elif target_job_title == 'AI':
        keyword_dict = pd.read_csv('ai_ml_keywords.csv')
        
    
    keyword_total = list(keyword_dict.count())
    total_sum = 0
    for i in keyword_total:
        total_sum = total_sum + i
        
    keyword_dict_col_names = list(keyword_dict.columns)
    
   
    matcher = PhraseMatcher(nlp.vocab)
    
    for col in keyword_dict_col_names: 
        matcher.add(col, None, *[nlp(text) for text in keyword_dict[col].dropna(axis = 0)])
    
    doc = nlp(text)
    print('doc' ,doc)
    
    d = []  
    matches = matcher(doc)
    for match_id, start, end in matches:
        rule_id = nlp.vocab.strings[match_id]  # get the unicode ID, i.e. 'COLOR'
        span = doc[start : end]  # get the matched slice of the doc
        d.append((rule_id, span.text))  
    keywords = "\n".join('{i[0]} {i[1]} ({j})' for i,j in Counter(d).items())
    
    ## convertimg string of keywords to dataframe
    df = pd.read_csv(StringIO(keywords),names = ['Keywords_List'])
    df1 = pd.DataFrame(df.Keywords_List.str.split(' ',1).tolist(),columns = ['Subject','Keyword'])
    df2 = pd.DataFrame(df1.Keyword.str.split('(',1).tolist(),columns = ['Keyword', 'Count'])
    df3 = pd.concat([df1['Subject'],df2['Keyword'], df2['Count']], axis =1) 
    df3['Count'] = df3['Count'].apply(lambda x: x.rstrip(")"))
    
    base = os.path.basename(file)
    filename = os.path.splitext(base)[0]
       
    name = filename.split('_')
    name2 = name[0]
    name2 = name2.lower()
    ## converting str to dataframe
    name3 = pd.read_csv(StringIO(name2),names = ['Candidate Name'])
    
    dataf = pd.concat([name3['Candidate Name'], df3['Subject'], df3['Keyword'], df3['Count']], axis = 1)
    dataf['Candidate Name'].fillna(dataf['Candidate Name'].iloc[0], inplace = True)
   # print(dataf)
    return(dataf,total_sum, )
    


#--------------------------------------

@app.route("/")
def index():
    return render_template("index.html")


@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/service_1")
def service_1():
    return render_template("service_1.html")

@app.route("/service_2")
def service_2():
    return render_template("service_2.html")


@app.route("/service_3")
def service_3():
    return render_template("service_3.html")

@app.route("/service_4")
def service_4():
    return render_template("service_4.html")





@app.route("/service_5")
def service_5():
 questions_shuffled = shuffle(questions)
 for i in questions.keys():
  random.shuffle(questions[i])
 return render_template('service_5_selection.html', q = questions_shuffled, o = questions)



@app.route('/quiz', methods=['POST'])
def quiz():
 print('eer  0', request.form)
 dropdown_selection = str(request.form)
 dropdown_selection = dropdown_selection.split()
 dropdown_selection = dropdown_selection[1]
 print('lord ', dropdown_selection)


 if 'DS' in dropdown_selection:
     questions_shuffled = shuffle(data_science_questions)
     for i in data_science_questions.keys():
      random.shuffle(data_science_questions[i])
     return render_template('service_5.html', q = questions_shuffled, o = data_science_questions, id = 'DS')

 if 'JL' in dropdown_selection:
     questions_shuffled = shuffle(java_lang_questions)
     for i in java_lang_questions.keys():
      random.shuffle(java_lang_questions[i])
     return render_template('service_5.html', q = questions_shuffled, o = java_lang_questions, id='JL')


@app.route('/quiz_result', methods=['POST'])
def quiz_answers():
 correct = 0
 print('eer  0', request.form)
 dropdown_selection = str(request.form)

 if 'JL' in dropdown_selection:
     for i in java_lang_questions.keys():
         answered = request.form[i]
         if java_lang_mcq[i][0] == answered:
             correct = correct + 1
     return render_template('quiz_result.html', correct = (correct/5)*100)
 if 'DS' in dropdown_selection:
     for i in data_science_questions.keys():
      answered = request.form[i]
      if data_science_mcq[i][0] == answered:
       correct = correct+1
     return render_template('quiz_result.html', correct = (correct/10)*100)
 #return '<h1>Correct Answers: <u>'+str(correct)+'</u></h1>'







@app.route("/test")
def test():
    return render_template("resume_score.html")


@app.route("/upload", methods=['POST'])
def upload():
    print('eer  0', request.form)
    dropdown_selection = str(request.form)
    dropdown_selection = dropdown_selection.split()
    dropdown_selection = dropdown_selection[1]
    
    if 'XMEN' in dropdown_selection:
        return ('Your are not an X men. You can never be.')
    
 
        
    
    target = 'images/'
    print('tt' , target)

    if not os.path.isdir(target):
        os.mkdir(target)

    for file in request.files.getlist("file"):
        print(file)
        filename = file.filename
        destination = "/".join([target, filename])
        print('des',destination)
        file.save(destination)
        
        
        
        
    mypath = os. getcwd()
    onlyfiles = [os.path.join(mypath, f) for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
    
    
    
    
    
    final_database=pd.DataFrame()
    i = 0 
    while i < 1:
        file = destination
        if 'WD' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'WD')
            selection = 'Web Developer'
        if 'DS' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'DS')
            selection = 'Data Scientist'
            
        if 'ISA' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'ISA')
            selection = 'Information Security Analyst'
            
        if 'SE' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'SE')
            selection = 'Software Engineer'
            
        if 'AI' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'AI')
            selection = 'AI and ML Engineer'
        final_database = final_database.append(dat)
        i +=1
        
        

    #=========================================
    
    final_database2 = final_database['Keyword'].groupby([final_database['Candidate Name'], final_database['Subject']]).count().unstack()
    final_database2.reset_index(inplace = True)
    final_database2.fillna(0,inplace=True)
    print(dat)
    print(dat['Keyword'])

    cand_key = dat['Keyword'].tolist()
    cand_key_new = []
    for i in cand_key:
        cand_key_new.append(i.strip())

    print('cc ', cand_key_new)
    
    final_database_col = list(final_database2.columns)
    final_database_col.pop(0)
    sum = 0
    for i in final_database_col:
        sum = sum + final_database2[i]
        
    sum = int(sum)
    resume_score = (sum/total_sum) * 100 
    resume_score = math.floor(resume_score)
    print(total_sum)
    gg= (set(rr) - set(cand_key_new))
    print(set(rr) - set(cand_key_new))
   
    return render_template('service_1_result.html', result = resume_score, resultKeywords = list(gg), selection =selection )






@app.route("/upload_2", methods=['POST'])
def upload_2():
    
    print('eer  0', request.form)
    dropdown_selection = str(request.form)
    dropdown_selection = dropdown_selection.split()
    dropdown_selection = dropdown_selection[1]
    
    if 'XMEN' in dropdown_selection:
        return ('Your are not an X men. You can never be.')
    
 
        
    
    target = 'images/'
    print('tt' , target)

    if not os.path.isdir(target):
        os.mkdir(target)

    for file in request.files.getlist("file"):
        print(file)
        filename = file.filename
        destination = "/".join([target, filename])
        print('des',destination)
        file.save(destination)
        
        
        
        
    mypath = os. getcwd()
    onlyfiles = [os.path.join(mypath, f) for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
    
    
    
    
    
    final_database=pd.DataFrame()
    i = 0 
    while i < 1:
        file = destination
        if 'WD' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'WD')
            selection = 'Web Developer'
        if 'DS' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'DS')
            selection = 'Data Scientist'
            
        if 'ISA' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'ISA')
            selection = 'Information Security Analyst'
            
        if 'SE' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'SE')
            selection = 'Software Engineer'
            
        if 'AI' in dropdown_selection:
            dat, total_sum,rr = service_2_profile(file, 'AI')
            selection = 'AI and ML Engineer'
        final_database = final_database.append(dat)
        i +=1
        
        

    #=========================================
    
    final_database2 = final_database['Keyword'].groupby([final_database['Candidate Name'], final_database['Subject']]).count().unstack()
    final_database2.reset_index(inplace = True)
    final_database2.fillna(0,inplace=True)
    print(dat)
    print(dat['Keyword'])

    cand_key = dat['Keyword'].tolist()
    cand_key_new = []
    for i in cand_key:
        cand_key_new.append(i.strip())

    print('cc ', cand_key_new)
    
    final_database_col = list(final_database2.columns)
    final_database_col.pop(0)
    sum = 0
    for i in final_database_col:
        sum = sum + final_database2[i]
        
    sum = int(sum)
    resume_score = (sum/total_sum) * 100 
    resume_score = math.floor(resume_score)
    print(total_sum)
    gg= (set(rr) - set(cand_key_new))
    print(set(rr) - set(cand_key_new))
        
        
   
    return render_template('service_2_result.html',result = gg, selection =selection )


@app.route('/addrec', methods=['POST', 'GET'])
def addrec():
    #return 'hello'

    gmailaddress = 'resumeit444@gmail.com'
    gmailpassword = 'zuRAd08080808'

    if request.method == 'POST':
        try:
            print('in try')
            mm = """\
Subject: Hi there

This message is sent from Python."""

            msgg = """\
From: Resume IT - 100 Days 100 Questions
Subject: Welcome!

We are glad that you subscribed for our 100 Days 100 Questions Challenge.
            
Let's Get Started
            
Question 1
Level 1
            
Question: Write a program which will find all such numbers which are divisible by 7 but are not a multiple of 5, between 2000 and 3200 (both included). The numbers obtained should be printed in a comma-separated sequence on a single line.
            
Hints: Consider use range(#begin, #end) method

            """

            mail = request.form['mail']
            print(mail)
            mailto = str(mail)
            mailServer = smtplib.SMTP('smtp.gmail.com', 587)
            mailServer.starttls()
            mailServer.login(gmailaddress, gmailpassword)
            mailServer.sendmail(gmailaddress, mailto, msgg)
            print(" \n Sent!")
            mailServer.quit()









        finally:
            print('in final')
            print(type(mail))
            import sqlite3
            conn = sqlite3.connect('resume_IT_db.sqlite')
            cur = conn.cursor()
            cur.execute('INSERT INTO subscribers (email) VALUES (?)',
                        [mail])
            conn.commit()


            conn = sqlite3.connect('resume_IT_db.sqlite')
            cur = conn.cursor()
            cur.execute('select * from subscribers')
            data = cur.fetchall()
            print('data ',data)
            global mailing_list
            mailing_list = data


            return render_template("service_3_result.html")

def send_regular_emails():
    def send_email():
        gmailaddress = 'bhuveshsharma09@gmail.com'
        gmailpassword = 'zuRAd09090909'
        email_user = 'myemail@gmail.com'
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(gmailaddress, gmailpassword)

        # EMAIL
        message = 'sending this from python!'
        server.sendmail(gmailaddress, mailing_list[0], message)
        server.quit()

    def send_email_at(send_time):
        time.sleep(send_time.timestamp() - time.time())
        send_email()
        print('email sent')

    first_email_time = dt.datetime(2020, 9, 19, 23, 32, 0)  # set your sending time in UTC
    interval = dt.timedelta(minutes=2 * 60)  # set the interval for sending the email

    send_time = first_email_time
    while 1:
        print('sending')
        send_email_at(send_time)
        send_time = send_time + interval

@app.route("/upload_3", methods=['POST'])
def upload_3():
    print('eer  0', request.form)
    dropdown_selection = str(request.form)
    dropdown_selection = dropdown_selection.split()
    dropdown_selection = dropdown_selection[1]

    if 'XMEN' in dropdown_selection:
        return ('Your are not an X men. You can never be.')

    target = 'images/'
    print('tt', target)

    if not os.path.isdir(target):
        os.mkdir(target)

    for file in request.files.getlist("file"):
        print(file)
        filename = file.filename
        destination = "/".join([target, filename])
        print('des', destination)
        file.save(destination)

    mypath = os.getcwd()
    onlyfiles = [os.path.join(mypath, f) for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]

    final_database = pd.DataFrame()
    i = 0
    while i < 1:
        file = destination
        if 'WD' in dropdown_selection:
            dat, total_sum, rr = service_2_profile(file, 'WD')
            selection = 'Web Developer'
        if 'DS' in dropdown_selection:
            dat, total_sum, rr = service_2_profile(file, 'DS')
            selection = 'Data Scientist'

        if 'ISA' in dropdown_selection:
            dat, total_sum, rr = service_2_profile(file, 'ISA')
            selection = 'Information Security Analyst'

        if 'SE' in dropdown_selection:
            dat, total_sum, rr = service_2_profile(file, 'SE')
            selection = 'Software Engineer'

        if 'AI' in dropdown_selection:
            dat, total_sum, rr = service_2_profile(file, 'AI')
            selection = 'AI and ML Engineer'
        final_database = final_database.append(dat)
        i += 1

    # =========================================

    final_database2 = final_database['Keyword'].groupby(
        [final_database['Candidate Name'], final_database['Subject']]).count().unstack()
    final_database2.reset_index(inplace=True)
    final_database2.fillna(0, inplace=True)
    print(dat)
    print(dat['Keyword'])

    cand_key = dat['Keyword'].tolist()
    cand_key_new = []
    for i in cand_key:
        cand_key_new.append(i.strip())

    print('cc ', cand_key_new)

    final_database_col = list(final_database2.columns)
    final_database_col.pop(0)
    sum = 0
    for i in final_database_col:
        sum = sum + final_database2[i]

    sum = int(sum)
    resume_score = (sum / total_sum) * 100
    resume_score = math.floor(resume_score)
    print(total_sum)
    gg = (set(rr) - set(cand_key_new))
    print(set(rr) - set(cand_key_new))

    jobs = []
    try:
        from googlesearch import search
    except ImportError:
        print("No module named 'google' found")

        # to search
    for word in cand_key_new:
        query = selection + " jobs nearby " + word

        for j in search(query, tld="co.in", num=1, stop=1, pause=2):
            print(j)
            jobs.append(j)

    return render_template('service_4_result.html', job_list = jobs )



if __name__ == "__main__":

    app.run()

    
    
    