# note:
# 1) change driver_path as per the local machine
# 2) change the resume file link as per your file for functionality testing


from selenium import webdriver
import time

host = 'http://127.0.0.1:5000/'
# set the driver path
driver_path = 'C:/Users/65909/Desktop/chromedriver'


def setup(path=driver_path, host=host):
    local_host = host
    web_driver = webdriver.Chrome(path)
    web_driver.get(local_host)
    return web_driver


def test_home_page(driver):
    time.sleep(2)
    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    time.sleep(2)


def test_goto_about_page(driver):
    # go to about_page from home_page
    about_button = driver.find_element_by_id('about_button')
    about_button.click()
    time.sleep(2)


def test_goto_home_page(driver):
    # go back to home_page from about_page
    home_button = driver.find_element_by_id('home_button')
    home_button.click()
    time.sleep(2)


def test_goto_service_1(driver):
    # go to service-1 page from home page
    service = driver.find_element_by_id('service')
    service.click()
    time.sleep(1)
    service_1 = driver.find_element_by_id('service_1')
    service_1.click()
    time.sleep(1)


def test_goto_service_2(driver):
    # go to service-1 page from home page
    service = driver.find_element_by_id('service')
    service.click()
    time.sleep(1)
    service_2 = driver.find_element_by_id('service_2')
    service_2.click()
    time.sleep(1)


def test_goto_service_3(driver):
    # go to service-1 page from home page
    service = driver.find_element_by_id('service')
    service.click()
    time.sleep(1)
    service_3 = driver.find_element_by_id('service_3')
    service_3.click()
    time.sleep(1)


def test_goto_service_4(driver):
    # go to service-1 page from home page
    service = driver.find_element_by_id('service')
    service.click()
    time.sleep(1)
    service_4 = driver.find_element_by_id('service_4')
    service_4.click()
    time.sleep(1)


def test_goto_service_5(driver):
    # go to service-1 page from home page
    service = driver.find_element_by_id('service')
    service.click()
    time.sleep(1)
    service_5 = driver.find_element_by_id('service_5')
    service_5.click()
    time.sleep(1)


def service_1_operation(driver):
    test_goto_service_1(driver=driver)
    file_picker = driver.find_element_by_id('file-picker').send_keys("C:/Users/65909/Desktop/resume-2.pdf")
    time.sleep(1)

    el = driver.find_element_by_name('select1')
    for option in el.find_elements_by_tag_name('option'):
        print(option.text)
        if option.text == 'Software Engineer':
            option.click()  # select() in earlier versions of webdriver
            break

    upload_button = driver.find_element_by_id('upload-button')
    upload_button.click()
    time.sleep(2)
    test_goto_home_page(driver =  driver)


def service_2_operation(driver):
    test_goto_service_2(driver=driver)
    file_picker = driver.find_element_by_id('file-picker').send_keys("C:/Users/65909/Desktop/resume-2.pdf")
    time.sleep(1)

    el = driver.find_element_by_name('select1')
    for option in el.find_elements_by_tag_name('option'):
        print(option.text)
        if option.text == 'Software Engineer':
            option.click()  # select() in earlier versions of webdriver
            break

    upload_button = driver.find_element_by_id('upload-button')
    upload_button.click()
    time.sleep(2)
    test_goto_home_page(driver=driver)

def service_4_operation(driver):
    test_goto_service_4(driver=driver)
    input_field = driver.find_element_by_id('input-box').send_keys("test@gmail.com")
    time.sleep(1)

    upload_button = driver.find_element_by_id('submit')
    upload_button.click()
    time.sleep(2)
    test_goto_home_page(driver=driver)


browser = setup()

service_4_operation(driver=browser)

# test cases to check buttons and links
test_home_page(driver=browser)
test_goto_about_page(driver=browser)
test_goto_home_page(driver=browser)
test_goto_service_1(driver=browser)
test_goto_service_2(driver=browser)
test_goto_service_3(driver=browser)
test_goto_service_4(driver=browser)
test_goto_service_5(driver=browser)
test_goto_home_page(driver=browser)

# test cases to test functionality of the services.
service_1_operation(driver=browser)
service_2_operation(driver=browser)
service_4_operation(driver=browser)