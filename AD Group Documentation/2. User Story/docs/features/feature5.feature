Feature: Home Page - Upload Resume Button
 As a User
 I can upload my resume, in PDF form, on website's home page
 So I can use various services on website

  Background:
   Given a User named "Bob"

  Scenario: Mr. Bob goes to 'ResumeIT' website's Home page and wants to upload resume
    Given Mr. Bob puts URL of website's home page in web browser and hits enter
    Then Mr. Bob should land at website's home page, when he will find a button 'Upload Resume' which will help him to upload the resume.
