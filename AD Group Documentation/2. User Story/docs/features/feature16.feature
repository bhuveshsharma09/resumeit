
Feature: Download Service result in PDF form locally:
 As a User,
 When I use any service at website, I will get my results and a button 'Download Results' will appear,
 So I can download results or feedback from the service at my computer for future reference
