Feature: Website - Home Page
 As a User,
 I can visit the home page of 'ResumeIT' website,
 So I can read the various features of website

  Background:
    Given a User named "Bob"

  Scenario: Mr. Bob goes to 'ResumeIT' website
    Given Mr. Bob puts URL of website in weh browser and hits enter
    Then Mr. Bob should land at 'ResumeIT' home page which will have details of all the features.
