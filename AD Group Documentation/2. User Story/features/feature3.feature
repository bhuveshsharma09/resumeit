Feature: Email feature at About-Us page
 As a User,
 I can click at 'Send Email' button from About-Us page
 So I can send an email to the organisation

 Background:
  Given a User named "Bob"

  Scenario: Mr. Bob goes to 'ResumeIT' website's About-Us page
    Given Mr. Bob search puts URL of website's About-Us page in web browser and hits enter and lands at About-Us page
    Then Mr. Bob should be able to see 'Send Email' button, which helps him to send an email to the organisation.

