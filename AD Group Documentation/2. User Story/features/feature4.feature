
Feature: Website - Services Page
 As a User
 I can visit the Services page of 'ResumeIT' website
 So I can chose between the services and read the content

  Background:
   Given a User named "Bob"

  Scenario: Mr. Bob goes to 'ResumeIT' website's Services page
    Given Mr. Bob puts URL of website's Services page in web browser and hits enter
    Then Mr. Bob should land at website's Services page, which will have details of all the services.
