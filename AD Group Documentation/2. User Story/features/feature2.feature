Feature: Website - About Us Page
 As a User
 I can visit the about-us page of 'ResumeIT' website
 So I can learn about the website

  Background:
    Given a User named "Bob"

  Scenario: Mr. Bob goes to 'ResumeIT' website's About-Us page
    Given Mr. Bob puts URL of website's About-Us page in web browser and hits enter
    Then Mr. Bob should land at website's About-Us page, which will have details of website creators.
