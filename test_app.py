import pytest
import app as flask_app
import pandas as pd
#from app import app as flask_app


# testing the response of page for index function
def test_index():
    response = flask_app.app.test_client().get('/')
    assert response.status_code == 200

def test_about():
    response = flask_app.app.test_client().get('/about')
    assert response.status_code == 200


def test_service_1():
    response = flask_app.app.test_client().get('/service_1')
    assert response.status_code == 200


def test_service_2():
    response = flask_app.app.test_client().get('/service_2')
    assert response.status_code == 200


def test_service_3():
    response = flask_app.app.test_client().get('/service_3')
    assert response.status_code == 200


def test_service_4():
    response = flask_app.app.test_client().get('/service_4')
    assert response.status_code == 200

def test_service_5():
    response = flask_app.app.test_client().get('/service_5')
    assert response.status_code == 200








data = {'Candidate Name':  ['test', 'test', 'test'],
        'Subject': ['DataEngineering', 'PythonLanguage', 'RLanguage'],
        'Keyword':['aws','python','r'],
        'Count':[1,1,1]      
        }

test_dataframe = pd.DataFrame (data, columns = ['Candidate Name','Subject','Keyword','Count'])
test_dataframe = test_dataframe.values.tolist()



def test_pdfextract():
    file='test_pdf.pdf'
    print(file)
    assert flask_app.pdfextract(file) == ['Hello World!\n \nA\nws,\n \npython, r\n \n']

    
def test_service_2_profile():
    file = 'test_pdf.pdf'
    target_job_title = 'DS'
    keywords = ['aws', 'nlp', 'python   ', 'flask', 'django', 'python', 'matplotlib', \
                'sklearn', 'r', 'shiny', 'Price', 'libraries', 'cnn', 'android applications', \
                 'project', 'linear', 'forest regression', 'statistical models']
    dataframe, number_of_total_keywords, list_of_keywords = flask_app.service_2_profile(file,target_job_title)
    dataframe = dataframe.values.tolist()
   
    assert dataframe.sort() == test_dataframe.sort()
    assert number_of_total_keywords == 18
    assert list_of_keywords == keywords
    
    

def test_service_1_profile():
    file = 'test_pdf.pdf'
    target_job_title = 'DS'
    keywords = ['aws', 'nlp', 'python   ', 'flask', 'django', 'python', 'matplotlib', \
                'sklearn', 'r', 'shiny', 'Price', 'libraries', 'cnn', 'android applications', \
                 'project', 'linear', 'forest regression', 'statistical models']
    dataframe, number_of_total_keywords = flask_app.service_1_profile(file,target_job_title)
    dataframe = dataframe.values.tolist()
   
    assert dataframe.sort() == test_dataframe.sort()
    assert number_of_total_keywords == 18
























