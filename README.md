<p align=center>

  <img src="https://drive.google.com/uc?export=download&id=1ZGlQYhmzmVNZ9XOv47emhVOe5G-K8W5p"/>

  <br>
  
  <br>
  <a target="_blank" href="https://www.python.org/downloads/" title="Python version"><img src="https://img.shields.io/badge/python-%3E=_3.6-green.svg"></a>

  
  <a target="_blank" href="https://gitlab.com/bhuveshsharma09/resumeit/-/jobs" title="Test Status"><img src="https://github.com/sherlock-project/sherlock/workflows/Tests/badge.svg?branch=master"></a>

 
  <a target="_blank" href="https://resume-it-20.herokuapp.com/"><img alt="Website" src="https://img.shields.io/website-up-down-green-red/http/sherlock-project.github.io/..svg"></a>

  




## Installation

```console
# clone the repo
$ git clone https://gitlab.com/bhuveshsharma09/resumeit.git

# change the working directory to resumeit
$ cd resumeit

# install the requirements
$ python3 -m pip install -r requirements.txt
```



## Usage

```console
$ python app.py

```
